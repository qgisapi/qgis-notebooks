## QGIS notebooks

This repository contains some JuPyteR and Rmarkdown example notebooks demonstrating the use of automatic report generation tools in combination with geospatial libraries. The examples focus on QGIS-specific features which are exposed by the Network API, and how they can be used together with other libraries such as `gdal`.
